<?php

namespace Test;

use App\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    public function testDivide()
    {
        $calculator = new Calculator(15, 5);
        ob_start();
        $calculator->print();
        $result = ob_get_clean();
        $this->assertEquals("3\n", $result);
    }
}
