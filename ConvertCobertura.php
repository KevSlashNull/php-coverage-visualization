<?php

$xml = new SimpleXMLElement(file_get_contents('cobertura.xml'));

$source = (string) $xml->sources->source[0];

unset($xml->sources);

$source = str_replace(__DIR__ . '/', '', $source);

foreach ($xml->packages as $package) {
    foreach ($package->package->classes as $class) {
        $class->class[0]->attributes()['filename'] = $source . '/' . $class->class[0]->attributes()['filename'];
    }
}

file_put_contents('cobertura.out.xml', $xml->asXML());
