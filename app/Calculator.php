<?php

namespace App;

class Calculator
{
    private int $a;
    private int $b;

    public function __construct(int $a, int $b)
    {
        $this->a = $a;
        $this->b = $b;
    }
    private function divide(): ?float
    {
        if ($this->b === 0) {
            return null;
        }

        return $this->a / $this->b;
    }

    public function print()
    {
        $result = $this->divide();

        if ($result === null) {
            echo "You can’t divide by zero." . PHP_EOL;
        } else {
            echo $this->divide() . PHP_EOL;
        }
    }

    public function subtract(): int
    {
        return $this->a - $this->b;
    }
}
